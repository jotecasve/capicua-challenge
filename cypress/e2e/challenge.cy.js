import '../support/commands';

Cypress.on('uncaught:exception', function (err, runnable) {
  return false;
});

describe('E2E test for the QA tools form', function () {

  beforeEach(function (){
    cy.fixture('data').then(function (testdata){
      this.testdata = testdata;
    })
    cy.visit('https://demoqa.com/automation-practice-form')
  })

  it('Validation of the red marked input fields', function () {
    cy.get('#submit').click({force: true})

    //Validation of first name: red marked enabled with empty value and green enabled after a type
    cy.get('#firstName').should('have.css', 'border-color', 'rgb(220, 53, 69)')
    cy.get('#firstName').click().type(this.testdata.firstName)
    cy.get('#firstName').should('have.css', 'border-color', 'rgb(40, 167, 69)')

    //Validation of last name: red marked enabled with empty value and green enabled after a type
    cy.get('#lastName').should('have.css', 'border-color', 'rgb(220, 53, 69)')
    cy.get('#lastName').click().type(this.testdata.firstName)
    cy.get('#lastName').should('have.css', 'border-color', 'rgb(40, 167, 69)')

    //Validation of gender: red marked enabled with empty value and green enabled after a click
    cy.contains('Male').should('have.css', 'color', 'rgb(220, 53, 69)')
    cy.get('#gender-radio-1').click({force: true})
    cy.contains('Male').should('have.css', 'color', 'rgb(40, 167, 69)')

    //Validation of phone number: red marked enabled with empty and textword values and green enabled after a type number
    cy.get('#userNumber').should('have.css', 'border-color', 'rgb(220, 53, 69)')
    cy.get('#userNumber').click().type("This is text")
    cy.get('#userNumber').should('have.css', 'border-color', 'rgb(220, 53, 69)')
    cy.get('#userNumber').clear().click().type(this.testdata.phoneNumber)
    cy.get('#userNumber').should('have.css', 'border-color', 'rgb(40, 167, 69)')

  })

  it('Validation of the form successfully filled', function () {
    cy.get('#firstName').click().type(this.testdata.firstName)
    cy.get('#lastName').click().type(this.testdata.lastName)
    cy.get('#userEmail').click().type(this.testdata.email)
    cy.get('#gender-radio-1').click({force: true})
    cy.get('#userNumber').click().type(this.testdata.phoneNumber)
    cy.get('#dateOfBirthInput').click().type("31 Aug 2022")
    cy.get('#subjectsInput').click({force: true}).type("This is a test")
    cy.get('#hobbies-checkbox-1').click({force: true})
    cy.get('#uploadPicture').selectFile('cypress/fixtures/images/image.png', { action: 'drag-drop' })
    cy.get('#currentAddress').click({force: true}).type("Breton de los Herreros 3")
    cy.get('#react-select-3-input').click({force: true}).type("NCR")
    cy.get('#react-select-4-input').click({force: true})
    cy.get('#react-select-4-input').click({force: true}).type("Delhi",{force: true})
    cy.get('#submit').click({force: true})

    cy.get('table.table.table-dark.table-striped.table-bordered.table-hover').get('tr').should(($p) => {
      // should have found 11 tr elements
      expect($p).to.have.length(11)
      expect($p.eq(1)).to.contain(this.testdata.firstName)
      expect($p.eq(1)).to.contain(this.testdata.lastName)
      expect($p.eq(2)).to.contain(this.testdata.email)
      expect($p.eq(3)).to.contain(this.testdata.gender)
      expect($p.eq(4)).to.contain(this.testdata.phoneNumber)
    })
  })
})